#ifndef Items_H
#define Items_H

#include "Entity.h"


class Items : public Entity
{
public:
	Items();
	~Items();
	enum items { KEY, BOOK, ARROW };  //�tems del juego

	virtual void init();
	virtual void init(int x, int y);
	virtual void init(int graphic, int x, int y, int w, int h);

	virtual void render();
	virtual void update();


	bool isOfClass(std::string classType);
	std::string getClassName() { return "Items"; };

protected:
		int				items[3];
		ofImage*		mpGraphicImg;

};

#endif
