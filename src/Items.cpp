//Include our classes
#include "Items.h"
#include "singletons.h"


Items::Items() : Entity(){
	/*mFrame = 0;
	mCurrentFrameTime = 0;*/
	//mpSpeed = 300;

	//mpGraphicImg = new ofImage();
	//mpGraphicImg->load("coinItem.png");
}

Items::~Items(){
}

void Items::init(){

	Entity::init();
}
void Items::init(int x, int y){
	Entity::init(x, y);
}
void Items::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}


void Items::update() {
	Entity::update();

}

void Items::render() {
	if (!mpAlive) { return; }
	ofSetColor(255, 255, 255);
	mpGraphicImg->drawSubsection(mpRect.x - 20, mpRect.y - 20,
		mpGraphicRect.w, mpGraphicRect.h,
		mpGraphicRect.x, mpGraphicRect.y);

	Entity::render();
}


bool Items::isOfClass(std::string classType){
	if(classType == "Items" || 
		classType == "Entity"){
		return true;
	}
	return false;
}

