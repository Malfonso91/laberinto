#include "singletons.h"

//Include our classes
#include "SceneMenu.h"

SceneMenu::SceneMenu(): Scene(){ //Calls constructor in class Scene
}

SceneMenu::~SceneMenu(){
}

void SceneMenu::init(){
	Scene::init(); //Calls to the init method in class Scene
	mpGraphicID = sResManager->getGraphicID("test.png");
	mpGraphicID2 = sResManager->getGraphicID("lab.png");
	mpGraphicID3 = sResManager->getGraphicID("selectionArrow.png");
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpGraphicID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpGraphicID);

	mpGraphicRect2.x = 0;
	mpGraphicRect2.y = 0;
	mpGraphicRect2.w = sResManager->getGraphicWidth(mpGraphicID3);
	mpGraphicRect2.h = sResManager->getGraphicHeight(mpGraphicID3);

	mpText = new ofTrueTypeFont();
	mpText->loadFont("True Lies.ttf", 36);
	mpSelection = -1;
}


void SceneMenu::load(){
	Scene::load(); //Calls to the init method in class Scene
}

void SceneMenu::updateScene(){
	inputEvent();
}

void SceneMenu::drawScene(){
	ofSetColor(107, 142, 35);
	//ofDrawRectangle(50, 50, 100, 100);
	imgRender(mpGraphicID2, 200, 400, mpGraphicRect, 255);
	imgRender(mpGraphicID3, 620, 310 + mpSelection * 80, mpGraphicRect2, 255);

	ofSetColor(255, 0, 0);
	mpText->drawString("The Maze of death", 250, 60);
	mpText->drawString("New game", 700, 360);
	mpText->drawString("Continue", 700, 440);
	mpText->drawString("Exit", 700, 520);
	
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneMenu::inputEvent(){
	//if (key_released[13]) {	// ENTER
	//if (key_released[8]) {	// BACKSPACE
	//if (key_released[9]) {	// TAB
	//if (key_released[127]) {	// SUPR
	if (key_released[13]) { //Space
		sDirector->changeScene(SceneDirector::LEVEL);
	}
	if (key_released[27]) { //Exit
		sDirector->changeScene(SceneDirector::LEVEL);
	}

	if (key_released[13]) { //TAB
		if (mpSelection == 1) {
			sDirector->changeScene(SceneDirector::LEVEL);
		}
		if (mpSelection == 0) {

		}
		if (mpSelection == 2) {
			sDirector->exitGame();
		}
	}
		if (key_pressed['s']) {
			mpSelection++;
		}
		if (key_pressed['w']) {
			mpSelection--;
		}
		if (mpSelection < 0) {
			mpSelection = 2;
		}
		if (mpSelection > 2) {
			mpSelection = 0;
		}
}
