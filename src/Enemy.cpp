//Include our classes
#include "Enemy.h"
#include "singletons.h"


Enemy::Enemy() : Entity(){
	mpSpeed = 200;
	mpTimer = 0;
}

Enemy::~Enemy(){
}

void Enemy::init(){
	Entity::init();
	mpAlive = true;
}
void Enemy::init(int x, int y){
	Entity::init(x, y);
}
void Enemy::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}

void Enemy::render() {
	Entity::render();
}

void Enemy::update() {
	Entity::update();
}

void Enemy::updateGraphic() {
	if (mpDirection != NONE) {
		mCurrentFrameTime += global_delta_time;
		if (mCurrentFrameTime > 100) {
			mCurrentFrameTime = 0;
			mFrame++;
			if (mFrame >= 3) {
				mFrame = 0;
			}
		}
	}
	else {
		mFrame = 0;
		mCurrentFrameTime = 0;
	}
	int row = mpDirection - 1;
	if (row < 0) { row = 0; }
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
	mpGraphicRect.y = row*mpGraphicRect.h;
}

bool Enemy::getAlive()
{
	return mpAlive;
}

void Enemy::setAlive(bool alive)
{
	mpAlive = alive;
}

void Enemy::updateControls() {
	if (mpTimer == 0) {
		int prev_dir = mpDirection;
		while (mpDirection == prev_dir) {
			int enemy_move = rand() % 4;
			if (enemy_move == 0) {
				mpDirection = UP;
				mFrame = 0;
				
			}
			else if (enemy_move == 1) {
				mpDirection = DOWN;
				mFrame = 1;
			}
			else if (enemy_move == 2) {
				mpDirection = LEFT;
				mFrame = 2;
			}
			else if (enemy_move == 3) {
				mpDirection = RIGHT;
				mFrame = 3;
			}
			else if (mpDirection == NONE) {
				!mFrame;
			}
		}
		mpTimer = 2000;
	}else {
		mpTimer -= global_delta_time;
		if (mpTimer < 0) {
			mpTimer = 0;
		}
	}
		return;
	
}

bool Enemy::isOfClass(std::string classType){
	if(classType == "Enemy" || 
		classType == "Entity"){
		return true;
	}
	return false;
}


void Enemy::move() {
	if (mpMoving) {
		int y_aux = mpRect.y;
		int x_aux = mpRect.x;
		if (mpRect.x < mpXtoGo) {
			mpRect.x += mpSpeed*global_delta_time / 1000;
		}
		else if (mpRect.x > mpXtoGo) {
			mpRect.x -= mpSpeed*global_delta_time / 1000;
		}
		if (mpRect.y < mpYtoGo) {
			mpRect.y += mpSpeed*global_delta_time / 1000;
		}
		else if (mpRect.y > mpYtoGo) {
			mpRect.y -= mpSpeed*global_delta_time / 1000;
		}
		if ((x_aux > mpXtoGo && mpRect.x < mpXtoGo) ||
			(x_aux < mpXtoGo && mpRect.x > mpXtoGo)) {
			mpRect.x = mpXtoGo;
		}
		if ((y_aux > mpYtoGo && mpRect.y < mpYtoGo) ||
			(y_aux < mpYtoGo && mpRect.y > mpYtoGo)) {
			mpRect.y = mpYtoGo;
		}
		if (mpRect.x == mpXtoGo && mpRect.y == mpYtoGo) {
			mpMoving = false;
		}
	}
	return;
}
