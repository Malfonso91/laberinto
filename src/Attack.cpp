//Include our classes
#include "Attack.h"
#include "singletons.h"


Attack::Attack() : Entity(){
	/*mFrame = 0;
	mCurrentFrameTime = 0;*/
	//mpSpeed = 300;

	mpGraphicImg = new ofImage();
	mpGraphicImg->load("coinItem.png");
}

Attack::~Attack(){
}

void Attack::init(){

	Entity::init();
}
void Attack::init(int x, int y){
	Entity::init(x, y);
}
void Attack::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}


void Attack::update() {
	Entity::update();

}

void Attack::render() {
	if (!mpAlive) { return; }
	ofSetColor(255, 255, 255);
	mpGraphicImg->drawSubsection(mpRect.x - 20, mpRect.y - 20,
		mpGraphicRect.w, mpGraphicRect.h,
		mpGraphicRect.x, mpGraphicRect.y);

	Entity::render();
}


bool Attack::isOfClass(std::string classType){
	/*if(classType == "Attack" || 
		classType == "Entity"){
		return true;
	}*/
	return false;
}

