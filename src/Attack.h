#ifndef Attack_H
#define Attack_H

#include "Entity.h"


class Attack : public Entity
{
public:
	Attack();
	~Attack();
	enum attack { KEY, BOOK, ARROW };  //�tems del juego

	virtual void init();
	virtual void init(int x, int y);
	virtual void init(int graphic, int x, int y, int w, int h);

	virtual void render();
	virtual void update();


	bool isOfClass(std::string classType);
	std::string getClassName() { return "Attack"; };

protected:
		int				attack[3];
		ofImage*		mpGraphicImg;

};

#endif
